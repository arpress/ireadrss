//
//  iReadRSSFeedModel.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 24/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "iReadRSSTableCell.h"
#import "FeedParser.h"
#import "FeedSource.h"
#import "FeedItem.h"
#import "CoreDataManager.h"
#import "Growl/Growl.h"

@interface iReadRSSFeedModel : NSObject <NSTableViewDataSource, GrowlApplicationBridgeDelegate>

@property (nonatomic, strong) NSArray *newsCollection;
@property (nonatomic, strong) NSNumber *notOlderThan;

- (void)parseFeeds;
//- (NSData *)fetchImageFromURL:(NSString *)link;

- (iReadRSSFeedModel *)initWithCoreDataManager:(CoreDataManager *)manager;
- (NSURL *)handleFeedSchema:(NSString *)url;

@end
