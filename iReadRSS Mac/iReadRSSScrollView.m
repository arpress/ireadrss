//
//  iReadRSSScrollView.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 2/8/13.
//  Copyright (c) 2013 Gian Marco Toso. All rights reserved.
//

#import "iReadRSSScrollView.h"

@interface iReadRSSScrollView ()

@property (nonatomic) float scrollEndCounter;
@property (nonatomic) float lastOrigin;

@end

@implementation iReadRSSScrollView

@synthesize scrollEndCounter = _scrollEndCounter;
@synthesize lastOrigin = _lastOrigin;

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
}

- (void)reflectScrolledClipView:(NSClipView *)cView {
    [super reflectScrolledClipView:cView];
    
    //NSLog(@"%f", cView.documentVisibleRect.origin.y);
    
    if (self.lastOrigin == cView.documentVisibleRect.origin.y)
        self.scrollEndCounter++;
    else {
        self.lastOrigin = cView.documentVisibleRect.origin.y;
        self.scrollEndCounter = 0;
    }
    
    if (self.scrollEndCounter == 10) {
        //NSLog(@"Scroll Stopped");
    }
}

@end
