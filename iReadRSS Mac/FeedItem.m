//
//  FeedItem.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 25/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import "FeedItem.h"
#import "FeedSource.h"

#import "Preferences.h"

#import "NSString+Contains.h"

@interface FeedItem()
    @property BOOL isThreadRunning;
    @property (nonatomic) Preferences *prefs;
@end

@implementation FeedItem

@dynamic date;
@dynamic desc;
@dynamic downloadDate;
@dynamic downloadFile;
@dynamic image;
@dynamic link;
@dynamic newEntry;
@dynamic readableDate;
@dynamic readLater;
@dynamic title;
@dynamic source;
@dynamic feedContent;
@dynamic imageURL;

@synthesize itemImage = _itemImage;
@synthesize isThreadRunning = _isThreadRunning;
@synthesize prefs = _prefs;

- (Preferences *)prefs {
    if (!_prefs) {
        _prefs = [[Preferences alloc] init];
        [_prefs readPlist];
    }
    
    return _prefs;
}

- (NSImage *)itemImage {
    if (!_itemImage) {
        if (self.image != nil) {
            if (![self.prefs.shouldCacheImages boolValue]) {
                self.image = nil;
            } else {
                _itemImage = [[NSImage alloc] initWithData:self.image];
                return _itemImage;
            }
        }
        
        if (!self.isThreadRunning) [FeedItem fetchImageForItem:self];
        return [NSImage imageNamed:@"NSMultipleDocuments"];
    }

    return _itemImage;
}

- (void)setItemImage:(NSImage *)itemImage {
    _itemImage = itemImage;
    
    self.isThreadRunning = NO;
}

+ (FeedItem *) findFeedItemWithData:(NSDictionary *)data inContext:(NSManagedObjectContext *)context {
    FeedItem *item = nil;
    
    // First I setup the various objects needed for the fetch request
    NSFetchRequest *fetchStory = [[NSFetchRequest alloc] init];
    NSPredicate *fetchStoryPredicate1 = [NSPredicate predicateWithFormat:@"title = %@", [data valueForKey:@"title"]];
    NSPredicate *fetchStoryPredicate2 = [NSPredicate predicateWithFormat:@"readableDate = %@", [data valueForKey:@"internalDate"]];
    NSArray *predicates = @[fetchStoryPredicate1, fetchStoryPredicate2];
    NSError *error = nil;
    
    // Then I assign the entity and the predicate to the fetch request
    fetchStory.entity = [NSEntityDescription entityForName:@"FeedItem" inManagedObjectContext:context];
    fetchStory.predicate = [NSCompoundPredicate andPredicateWithSubpredicates:predicates];
    
    // Then I execute the request
    item = [[context executeFetchRequest:fetchStory error:&error] lastObject];
    
    if (error != nil) {   // Something went wrong with the database, I need to abort the whole parsing operation
        NSLog(@"Error reading from the database while fetching an item: %@", [error localizedDescription]);
        
        return nil;
    }
        
    return item;
}


+ (NSArray *) getItemsOlderThan:(NSUInteger)days inContext:(NSManagedObjectContext *)context {
    
    NSFetchRequest *harvest = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    NSTimeInterval t = days * 86400;
    
    NSDate *today = [NSDate date];
    NSDate *limit = [today dateByAddingTimeInterval:-(t)];
    
    // Then I assign the entity and the predicate to the fetch request
    harvest.entity = [NSEntityDescription entityForName:@"FeedItem" inManagedObjectContext:context];
    harvest.predicate = [NSPredicate predicateWithFormat:@"date < %@", limit];
    
    // Then I execute the request
    NSArray *toReturn = [context executeFetchRequest:harvest error:&error];
    
    return toReturn;
}

+ (FeedItem *) feedItemWithData: (NSDictionary *)data fromSource: (NSString *)sourceName inContext:(NSManagedObjectContext *)context shouldSkipSearch:(BOOL)skip notOlderThan:(NSUInteger)days
{
    FeedItem *item = nil;
    
    if (days != 0) {
        NSTimeInterval t = days * 86400;
        
        NSDate *today = [NSDate date];
        NSDate *limit = [today dateByAddingTimeInterval:-(t)];
        
        if ([limit earlierDate:data[@"pubDate"]] != limit)
            return nil;
    }
    
    if (skip || !(item = [FeedItem findFeedItemWithData:data inContext:context])) {
        item = [NSEntityDescription insertNewObjectForEntityForName:@"FeedItem" inManagedObjectContext:context];
        
        item.date = data[@"pubDate"];
        item.title = data[@"title"];
        item.desc = data[@"description"];
        item.readableDate = data[@"internalDate"];
        item.source = [FeedSource feedSourceWithName:sourceName inContext:context];
        item.link = data[@"link"];
        item.feedContent = data[@"feedContent"];
        item.imageURL = data[@"imageURL"];
        
        //[FeedItem fetchImageForItem:item];
    }
    
    return item;
}

+ (NSArray *) getAllItemsInContext:(NSManagedObjectContext *)context {
    NSFetchRequest *fetchStory = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    
    // Then I assign the entity and the predicate to the fetch request
    fetchStory.entity = [NSEntityDescription entityForName:@"FeedItem" inManagedObjectContext:context];
    fetchStory.predicate = nil;
    
    // Then I execute the request
    NSArray *toReturn = [context executeFetchRequest:fetchStory error:&error];
    
    return toReturn;
}

- (NSData *)createThumbnailFromData:(NSData *)imageData {
    if (!imageData) return nil;
    
    CGImageSourceRef cgImg = CGImageSourceCreateWithData((__bridge CFDataRef)imageData, NULL);
    int size = 84;
    
    CFDictionaryRef options = NULL;
    CFStringRef keys[3];
	CFTypeRef values[3];
	CFNumberRef thumbSizeRef = CFNumberCreate(NULL, kCFNumberIntType, &size);
	keys[0] = kCGImageSourceCreateThumbnailFromImageIfAbsent;
	values[0] = (CFTypeRef)kCFBooleanTrue;
	keys[1] = kCGImageSourceThumbnailMaxPixelSize;
	values[1] = (CFTypeRef)thumbSizeRef;
    keys[2] = kCGImageSourceCreateThumbnailWithTransform;
    values[2] = (CFTypeRef)kCFBooleanTrue;
    options = CFDictionaryCreate(NULL, (const void **)keys, (const void **)values, 3, &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
    CFRelease(thumbSizeRef);
    
    CGImageRef thumbnailImageRef = CGImageSourceCreateThumbnailAtIndex(cgImg, 0, (CFDictionaryRef)options);
    
    NSImage *smaller = [[NSImage alloc] initWithCGImage:thumbnailImageRef size:NSMakeSize(CGImageGetWidth(thumbnailImageRef), CGImageGetHeight(thumbnailImageRef))];
    
    NSData *toReturn = [smaller TIFFRepresentation];
    
    CFRelease(cgImg);
    CFRelease(options);
    CGImageRelease(thumbnailImageRef);
    
    return toReturn;
}

+ (id) sendRequestTo:(NSURL *)theURL error:(NSError **)error shouldReturnData:(BOOL)returnData {
    NSString *theHTML = nil;
    int i = 0;
    
    NSStringEncoding encodings[] = {NSUTF8StringEncoding, NSISOLatin1StringEncoding, NSISOLatin2StringEncoding, NSASCIIStringEncoding };
    
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:theURL cachePolicy:NSURLCacheStorageAllowed timeoutInterval:30];
    NSURLResponse *theResponse = nil;
    
    NSData *connectionData = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&theResponse error:error];
    
    if (returnData)
        return connectionData;
    
    while (theHTML == nil && i < 5)
        theHTML = [[NSString alloc]initWithData:connectionData encoding:encodings[i++]];
    
    return theHTML;
}

- (NSTextCheckingResult *) scanWebPage: (NSString *)theHTML withPattern: (NSString *)pattern error:(NSError **)error
{
    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:pattern options:NSRegularExpressionCaseInsensitive error:error];
    
    return [expression firstMatchInString:theHTML options:0 range:NSMakeRange(0, [theHTML length])];
}

- (NSData *)getImageData:(NSString *)imageURL {
    NSError *error = nil;
    NSData *imageData = [FeedItem sendRequestTo:[NSURL URLWithString:imageURL] error:&error shouldReturnData:YES];
    
    if (error != nil)
        return nil;
    
    NSData *thumbnail = [self createThumbnailFromData: imageData];
    
    return thumbnail;
}

- (NSData *)fetchImageFromHTML:(NSString *)theHTML withURL:(NSURL *)theURL {
    NSError *error = nil;
    NSArray *findEntry = [theHTML componentsSeparatedByString:@"div class=\"entry\""];
    NSString *entryFound = [NSString stringWithString:[findEntry lastObject]];
    
    NSTextCheckingResult *match = [self scanWebPage:entryFound withPattern:@"<img[^>]+src\\s*=\\s*['\"]([^'\"]+)['\"][^>]*>" error:&error];
    
    NSString *imgTag = [theHTML substringWithRange:[match rangeAtIndex:1]];
    
    // Let's avoid images like "share this" or "email this", shall we? They are usually hosted on feedburner so I'm making a wild guess, but it appears to be good enough!
    if ([imgTag containsString:@"feedburner"])
        return nil;
    
    NSString *imageURL = [[imgTag componentsSeparatedByString:@"src=\""] lastObject];
        
    // This *might* be a relative path. I can try to fix it.
    NSArray *splitUrl = [imageURL componentsSeparatedByString:@"://"];
    
    if (![[imageURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length])
        return nil;
    
    if ([splitUrl count] == 1) { // It is relative, indeed
        imageURL = [NSString stringWithFormat:@"http://%@%@", [theURL host], imageURL];
    }
    
    if (imageURL != nil) {
        return [self getImageData:imageURL];
    } else {
        return nil;
    }
}

- (NSData *) fetchImageFromURL: (NSString *)link {
    NSError *error = nil;
    NSString *theHTML = nil;
    NSString *saneLink = [link stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    NSURL *theURL = [NSURL URLWithString:saneLink];
    
    theHTML = [FeedItem sendRequestTo:theURL error:&error shouldReturnData:NO];
    
    if (error != nil || theHTML == nil) {
        return nil;
    }
    
    return [self fetchImageFromHTML:theHTML withURL:theURL];
}

- (void)fetchImageThreadCallback:(NSDictionary *)args {
    @try {
        if ([self.source.shouldAutoDownload boolValue] && self.downloadFile != nil) {
            NSString *path = [self.source.downloadPath stringByAppendingPathComponent:self.downloadFile];
            
            self.itemImage = [[NSWorkspace sharedWorkspace] iconForFile:path];
            return;
        }
        
        NSData *image = nil;
        
        if (self.imageURL && ![[self.imageURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] isEqual: @""]) {
            image = [self getImageData:self.imageURL];
        } else if (self.feedContent) {
            image = [self fetchImageFromHTML:self.feedContent withURL:nil]; // [self fetchImageFromURL:self.link];
        }
        
        if (image == nil) {
            image = [self fetchImageFromURL:self.link];
        }
        
        if (image != nil) {
            self.itemImage = [[NSImage alloc] initWithData: image];
            self.image = image;
        } else {
            self.itemImage = [NSImage imageNamed:@"NSMultipleDocuments"];
        }
    }
    @catch (NSException *ex) {
        // If the source is deleted and this thread is running, we will end up here. Nothing else needs to be done, so we can safely ignore the exception and let the thread die an honorable death.
    }
    @finally {
        
    }
}

+ (void)fetchImageForItem:(FeedItem *)item {
    NSThread *parseThread = [[NSThread alloc] initWithTarget:item selector:@selector(fetchImageThreadCallback:) object:nil];
    [parseThread start];
    item.isThreadRunning = YES;
}

- (void)markAsRead:(NSNumber *)value {
    self.newEntry = [NSNumber numberWithBool:!value.boolValue];
}

- (void)bookmarkItem {
    self.readLater = [NSNumber numberWithBool:![self.readLater boolValue]];
}

@end
