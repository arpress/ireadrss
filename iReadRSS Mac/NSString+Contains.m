//
//  NSObject+NSString_Contains.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 1/20/13.
//  Copyright (c) 2013 Gian Marco Toso. All rights reserved.
//

#import "NSString+Contains.h"

@implementation NSString (NSString_Contains)

- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location != NSNotFound;
}

- (BOOL)containsString:(NSString *)string {
    return [self containsString:string options:0];
}

- (BOOL)containedStringLocation: (NSString *) string options:(NSStringCompareOptions)options {
    NSRange rng = [self rangeOfString:string options:options];
    return rng.location;
}

@end