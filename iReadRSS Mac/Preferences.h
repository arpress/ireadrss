//
//  Preferences.h
//  RSSReader
//
//  Created by Gian Marco Toso on 11/28/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

#define DEFAULT_FETCHINTERVAL           [NSNumber numberWithInt:5   ]
#define DEFAULT_DELETEITEMSOLDERTHAN    [NSNumber numberWithInt:14  ]
#define DEFAULT_SHOWBADGEONICON         [NSNumber numberWithInt:1   ]
#define DEFAULT_BOUNCEDOCKICON          [NSNumber numberWithInt:0   ]
#define DEFAULT_ISDEFAULTREADER         [NSNumber numberWithInt:0   ]
#define DEFAULT_USEGROWL                [NSNumber numberWithInt:0   ]
#define DEFAULT_GROUPGROWLNOTIFICATIONS [NSNumber numberWithInt:1   ]
#define DEFAULT_GROUPSOURCES            [NSNumber numberWithInt:0   ]
#define DEFAULT_DISABLEDLSNOTIFICATIONS [NSNumber numberWithInt:0   ]
#define DEFAULT_SHOULDCACHEIMAGES       [NSNumber numberWithInt:0   ]
#define DEFAULT_PLAYSOUNONNEWITEMS      [NSNumber numberWithInt:0   ]


@interface Preferences : NSObject {
    NSNumber *fetchInterval;
    NSNumber *deleteItemsOlderThan;
    NSNumber *showBadgeOnIcon;
    NSNumber *bounceDockIconOnNewItems;
    NSNumber *isDefaultReader;
    NSNumber *useGrowl;
    NSNumber *groupGrowlNotifications;
    NSNumber *groupSources;
    NSNumber *disableDlsNotifications;
    NSNumber *shouldCacheImages;
    NSNumber *playSoundOnNewItems;
}

@property (strong) NSNumber *fetchInterval;
@property (strong) NSNumber *deleteItemsOlderThan;
@property (strong) NSNumber *showBadgeOnIcon;
@property (strong) NSNumber *bounceDockIconOnNewItems;
@property (strong) NSNumber *isDefaultReader;
@property (strong) NSNumber *useGrowl;
@property (strong) NSNumber *groupGrowlNotifications;
@property (strong) NSNumber *groupSources;
@property (strong) NSNumber *disableDlsNotifications;
@property (strong) NSNumber *shouldCacheImages;
@property (strong) NSNumber *playSoundOnNewItems;

-(void)readPlist;
-(BOOL)savePlist;

@end
