//
//  iReadRSSAppDelegate.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 24/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import "iReadRSSAppDelegate.h"

@interface iReadRSSAppDelegate()

@property NSButton *scrollButton;

@end

@implementation iReadRSSAppDelegate

@synthesize managedObjectContext = _managedObjectContext;
@synthesize feedSortDescriptors = _feedSortDescriptors;
@synthesize prefs = _prefs;
@synthesize newFeedIsDownload = _newFeedIsDownload;
@synthesize scrollButton = _scrollButton;

- (Preferences *)prefs {
    if (!_prefs) {
        _prefs = [[Preferences alloc] init];
        [_prefs readPlist];
    }
    
    return _prefs;
}

- (BOOL)newFeedIsDownload {
    return (self.addFeedDownloadCheckbox.state == NSOnState) ? YES : NO;
}

- (NSArray *)feedSortDescriptors {
    if (!_feedSortDescriptors)
        _feedSortDescriptors = @[[NSSortDescriptor sortDescriptorWithKey:@"date"
                                           ascending:NO]];
    
    return _feedSortDescriptors;
}

- (void)openExternalURL:(NSAppleEventDescriptor *)event withReplyEvent:(NSAppleEventDescriptor *)replyEvent {
    NSString *urlStr = [[event paramDescriptorForKeyword:keyDirectObject]
                        stringValue];
    
    [self.addFeedURLField setStringValue:urlStr];
    [self.addFeedURLField setEnabled:NO];
    
    NSURL *sourceURL = [self.model handleFeedSchema:urlStr];
    FeedParser *parser = [[FeedParser alloc] initWithUrl:sourceURL];
    [parser parseFeed];
    
    if (parser.feedTitle)
        [self.addFeedNameField setStringValue:parser.feedTitle];
    
    [NSApp beginSheet:self.addFeedWindow modalForWindow:self.window modalDelegate:self didEndSelector:@selector(addSheetClosed) contextInfo:nil];
}

- (IBAction)addFeedModalWindowButtonClick:(id)sender {
    if ([sender tag] == 1) {
        [NSApp endSheet:self.addFeedWindow];
        [self.addFeedWindow orderOut:self];
        return;
    }
    
    NSString *newSourceURL = [self.addFeedURLField stringValue];
    NSString *newSourceName = [self.addFeedNameField stringValue];
    
    if (newSourceURL == nil || [[newSourceURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        return;
    
    if (newSourceName == nil || [[newSourceName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        return;
    
    FeedSource *newSource = [FeedSource feedSourceWithName:newSourceName andURL:newSourceURL inContext:[self.dataManager managedObjectContext]];
    
    if (self.newFeedIsDownload) {
        newSource.shouldAutoDownload = [NSNumber numberWithBool:YES];
        newSource.downloadPath = self.addFeedDownloadPath.selectedItem.title;
    }
    
    [self.dataManager saveData];
    
    [self.addFeedDownloadPath selectItemAtIndex:3];
    if (self.addFeedDownloadCheckbox.state == NSOnState) {
        self.addFeedDownloadCheckbox.state = NSOffState;
        [self checkAddFeedDownloadCheckbox:self];
    }
    
    [NSApp endSheet:self.addFeedWindow];
    [self.addFeedWindow orderOut:self];
}


- (void)addSheetClosed {
    [self.addFeedURLField setStringValue:@""];
    [self.addFeedNameField setStringValue:@""];
    
    [self.model parseFeeds];
}


- (void)applicationDidFinishLaunching:(NSNotification *)aNotification
{    
    self.dataManager = [[CoreDataManager alloc] init];
    self.managedObjectContext = self.dataManager.managedObjectContext;

    self.model = [[iReadRSSFeedModel alloc] initWithCoreDataManager:self.dataManager];
    self.model.notOlderThan = self.prefs.deleteItemsOlderThan;
    [self.model parseFeeds];
    
    [self applyPreferences];
    
    /* Add self as observer for some messages */
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(settingsWindowHasClosed) name:@"SettingsWindowHasClosed" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedParseBegin) name:@"FeedParseBegin" object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(feedParseEnd) name:@"FeedParseEnd" object:nil];
    
    /* Add self as observer for some KeyValues */
    [self.unreadItems addObserver:self forKeyPath:@"arrangedObjects" options:NSKeyValueSetSetMutation context:NULL];
    
    [self.tableView setDoubleAction:@selector(doubleClickCell:)];
    [self.downloadHistory setDoubleAction:@selector(doubleClickCell:)];
    
//    self.scrollButton = [[NSButton alloc] initWithFrame:NSMakeRect(0, -89, self.tableView.frame.size.width, 89)];
//    [self.scrollView.contentView addSubview:self.scrollButton positioned:NSWindowAbove relativeTo:self.tableView];
//    [self.scrollView.contentView setPostsBoundsChangedNotifications:YES];
//    
//    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(boundsDidChange:) name:NSViewBoundsDidChangeNotification object:self.scrollView.contentView];
    
    [self startTimer];
}

- (void)boundsDidChange:(NSView *)sender {
    float scrollY = self.scrollView.contentView.documentVisibleRect.origin.y;
    
    if (scrollY < -40)
        [self runParse:self];
}

- (void)doubleClickCell:(NSTableView *)sender {
    NSView *row = [sender rowViewAtRow:sender.clickedRow makeIfNecessary:NO];
    
    iReadRSSTableCell *cell = row.subviews[sender.clickedColumn];
    [cell doubleClick];
}

- (void)feedParseBegin {
    [self.updateIndicator startAnimation:self];
    [self.updateText setHidden:NO];
}

- (void)feedParseEnd {
    [self.updateIndicator stopAnimation:self];
    [self.updateText setHidden:YES];
    
    parseIsRunning = NO;
}

- (IBAction)openPreferencesWindow:(id)sender {
    if (!self.settingsWindowController)
        self.settingsWindowController = [[SettingsWindowController alloc] initInManagedContext:self.managedObjectContext];
    
    [NSApp runModalForWindow:[self.settingsWindowController window]];
}

- (IBAction)runParse:(id)sender {
    if (!parseIsRunning) {
        [self deleteOldEntries];
        
        [self.model parseFeeds];
        parseIsRunning = YES;
    }
}

- (IBAction)markAllItemsAsRead:(id)sender {
    [self.unreadItems.arrangedObjects makeObjectsPerformSelector:@selector(markAsRead:) withObject:[NSNumber numberWithBool:YES]];
    
    [self.unreadItemsArrayController fetch:self];
    self.tableView.needsDisplay = YES;
    self.downloadHistory.needsDisplay = YES;
}

- (IBAction)showPopOver:(NSButton *)sender {
    if (sender.state == NSOnState) {
        [self.popover showRelativeToRect:sender.bounds ofView:sender preferredEdge:NSMaxYEdge];
        self.downloadHistory.needsDisplay = YES;
    } else {
        [self.popover close];
    }
}

- (IBAction)markItemAsRead:(id)sender {
    if (self.tableView.selectedRow == -1) return;
    
    NSView *row = [self.tableView rowViewAtRow:self.tableView.selectedRow makeIfNecessary:NO];
    
    iReadRSSTableCell *cell = row.subviews[0];
    
    [cell performSelector:@selector(markItemAsRead:) withObject:[NSNumber numberWithBool:YES]];
}

- (IBAction)markItemAsUnread:(id)sender {
    if (self.tableView.selectedRow == -1) return;
    
    NSView *row = [self.tableView rowViewAtRow:self.tableView.selectedRow makeIfNecessary:NO];
    
    iReadRSSTableCell *cell = row.subviews[0];
    
    [cell performSelector:@selector(markItemAsRead:) withObject:[NSNumber numberWithBool:NO]];
    
}

- (IBAction)bookmarkItem:(id)sender {
    if (self.tableView.selectedRow == -1) return;
    
    NSView *row = [self.tableView rowViewAtRow:self.tableView.selectedRow makeIfNecessary:NO];
    
    iReadRSSTableCell *cell = row.subviews[0];

    [cell performSelector:@selector(bookmarkItem)];
}

- (IBAction)checkAddFeedDownloadCheckbox:(id)sender {
    [self.addFeedDownloadBox setHidden:!self.newFeedIsDownload];

    NSRect currentFrame = self.addFeedWindow.frame;
    
    NSRect newFrame = NSMakeRect(currentFrame.origin.x, currentFrame.origin.y, currentFrame.size.width, currentFrame.size.height + ((self.newFeedIsDownload) ? self.addFeedDownloadBox.frame.size.height - 10 : -self.addFeedDownloadBox.frame.size.height + 10));
    
    [self.addFeedWindow setFrame:newFrame display:YES animate:YES];
    
    if (self.addFeedDownloadPath.indexOfSelectedItem == 0) {
        [self.addFeedDownloadPath selectItemAtIndex:3];
    }
}

- (IBAction)openSelectedItem:(id)sender {
    NSView *row = nil;
    
    if ([self tableViewIsSelected]) {
        row = [self.tableView rowViewAtRow:self.tableView.selectedRow makeIfNecessary:NO];
    } else if ([self downloadHistoryIsSelected]) {
        row = [self.downloadHistory rowViewAtRow:self.downloadHistory.selectedRow makeIfNecessary:NO];
    } else return;
    
    iReadRSSTableCell *cell = row.subviews[0];
        
    [cell performSelector:@selector(doubleClick)];
}

- (void)startTimer {
    if (fetchTimer)
        [fetchTimer invalidate];
    
    fetchTimer = [NSTimer timerWithTimeInterval:([self.prefs.fetchInterval intValue] * 60) target:self selector:@selector(timerTick) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:fetchTimer forMode:NSDefaultRunLoopMode];
}

- (void)timerTick {
    [self runParse:self];
}

- (void)applyPreferences {
    [self.prefs readPlist];
    
    [self startTimer];
    
    if ([self.prefs.showBadgeOnIcon boolValue])
        [self displayBadge];
    else
        [[[NSApplication sharedApplication] dockTile] setBadgeLabel:nil];
    
    NSAppleEventManager *eventManager = [NSAppleEventManager sharedAppleEventManager];
    
    if ([self.prefs.isDefaultReader boolValue]) {
        [eventManager setEventHandler:self andSelector:@selector(openExternalURL:withReplyEvent:) forEventClass:kInternetEventClass andEventID:kAEGetURL];
        
        NSString *bundleID = [[NSBundle mainBundle] bundleIdentifier];
        LSSetDefaultHandlerForURLScheme(CFSTR("feed"), (__bridge CFStringRef)bundleID);
    } else {
        [eventManager removeEventHandlerForEventClass:kInternetEventClass andEventID:kAEGetURL];
        LSSetDefaultHandlerForURLScheme(CFSTR("feed"), CFSTR("com.apple.safari"));
    }
}

- (void) settingsWindowHasClosed {
    [self applyPreferences];
    
    [NSApp stopModal];

    [self.itemsArrayController fetch:self];
    [self.unreadItemsArrayController fetch:self];
    [self runParse:nil];
}

- (void) deleteOldEntries {
    int dayCount = [self.prefs.deleteItemsOlderThan intValue];
    
    if (dayCount == 0)
        return;
    
    NSArray *toDelete = [FeedItem getItemsOlderThan:[self.prefs.deleteItemsOlderThan intValue] inContext:self.managedObjectContext];
    
    for (NSUInteger i = 0; i < [toDelete count]; i++) {
        FeedItem *current = toDelete[i];
        
        if (![current.readLater boolValue])
            [self.managedObjectContext deleteObject:current];
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    if ([keyPath isEqualToString:@"arrangedObjects"] && object == self.unreadItems) {
        if ([self.prefs.showBadgeOnIcon boolValue])
            [self displayBadge];
    }
}

- (void)displayBadge {
    NSDockTile *dockTile = [[NSApplication sharedApplication] dockTile];
    NSUInteger currentCount = [[self.unreadItems arrangedObjects] count];
    NSString *label = [NSString stringWithFormat:@"%lu", currentCount];
    
    [dockTile setBadgeLabel:(currentCount > 0) ? label : nil];
}

// Returns the NSUndoManager for the application. In this case, the manager returned is that of the managed object context for the application.
- (NSUndoManager *)windowWillReturnUndoManager:(NSWindow *)window
{
    return [[self managedObjectContext] undoManager];
}

- (NSApplicationTerminateReply)applicationShouldTerminate:(NSApplication *)sender
{
    // Save changes in the application's managed object context before the application terminates.
    [self.dataManager saveData];
    
    return NSTerminateNow;
}

- (void) updateItemsArrayPredicate {
    NSMutableArray *filters = [NSMutableArray array];
    if (sourceFilter != nil)
        [filters addObject:sourceFilter];
    
    if (customFilter != nil)
        [filters addObject:customFilter];
    
    if (readLaterFilter != nil)
        [filters addObject:readLaterFilter];
    
    self.filterPredicateArray = [NSCompoundPredicate andPredicateWithSubpredicates:filters];
}

- (IBAction)filterSource:(id)sender {
    NSMenuItem *selected = [self.filterMenu selectedItem];
    
    if ([[selected title] isEqualToString:@"All Feeds"]) {
        sourceFilter = nil;
    } else {
        NSString *filterString = [selected title];
        sourceFilter = [NSPredicate predicateWithFormat:@"source.name = %@", filterString];
    }
    
    [self updateItemsArrayPredicate];
}

- (IBAction)filterCustomSearch:(id)sender {
    NSString *query = [sender stringValue];
    if ([[query stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0) {
        customFilter = nil;
    } else {
        NSPredicate *titlePredicate = [NSPredicate predicateWithFormat:@"title contains[c] %@", query];
        
        customFilter = titlePredicate;
    }
    
    [self updateItemsArrayPredicate];
}

- (IBAction)filterBookmarks:(NSButton *)sender {
    readLaterFilter = (sender.state == NSOnState) ? [NSPredicate predicateWithFormat:@"readLater = 1"] : nil;
    
    [self updateItemsArrayPredicate];
}

- (BOOL)applicationShouldHandleReopen:(NSApplication *)sender hasVisibleWindows:(BOOL)flag {
    if (!flag) {
        [self.window makeKeyAndOrderFront:self.window];
    }
    
    return YES;
}

- (void)tableViewSelectionIsChanging:(NSNotification *)notification {

}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    if ([self tableViewIsSelected])
        [self closeDownloadHistory];
}

- (BOOL)tableViewIsSelected {
    return (self.tableView == self.window.firstResponder && self.tableView.selectedRow != -1);
}

- (BOOL)downloadHistoryIsSelected {
    return (self.downloadHistory == self.window.firstResponder && self.downloadHistory.selectedRow != -1);
}

- (void)closeDownloadHistory {
    self.showDownloadHistoryButton.state = NSOffState;
    [self.popover close];
}

@end
