//
//  FeedSource.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 25/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import "FeedSource.h"
#import "FeedItem.h"


@implementation FeedSource

@dynamic autoOpen;
@dynamic autoRead;
@dynamic downloadPath;
@dynamic justDownload;
@dynamic name;
@dynamic shouldAutoDownload;
@dynamic status;
@dynamic url;
@dynamic items;

+ (FeedSource *) feedSourceWithName:(NSString *)name andURL:(NSString *)theURL inContext:(NSManagedObjectContext *)context {
    FeedSource *source = [FeedSource feedSourceWithName:name inContext:context];
    
    if (source != nil)
        return source;
    
    source = [NSEntityDescription insertNewObjectForEntityForName:@"FeedSource" inManagedObjectContext:context];
    
    source.name = name;
    source.url = theURL;
    
    return source;
}

+ (FeedSource *) feedSourceWithName:(NSString *)name inContext:(NSManagedObjectContext *)context
{
    //    NSLog(@"I am now fetching %@ from the database", name);
    
    FeedSource *source = nil;
    
    // First I setup the various objects needed for the fetch request
    NSFetchRequest *fetchSource = [[NSFetchRequest alloc] init];
    NSPredicate *fetchSourcePredicate = [NSPredicate predicateWithFormat:@"name = %@", name];
    NSError *error = nil;
    
    // Then I assign the entity and the predicate to the fetch request
    fetchSource.entity = [NSEntityDescription entityForName:@"FeedSource" inManagedObjectContext:context];
    fetchSource.predicate = fetchSourcePredicate;
    
    // Then I execute the request
    source = [[context executeFetchRequest:fetchSource error:&error] lastObject];
    
    if (error != nil) {   // Something went wrong with the database, I need to abort the whole parsing operation
        NSLog(@"Error reading from the database while fetching an item: %@", [error localizedDescription]);
        
        return nil;
    }
    
    if (source == nil) {
        return nil;
    }
    
    return source;
}

- (NSUInteger)unreadItems {
    NSUInteger count = 0;
    
    for (FeedItem *f in self.items) {
        BOOL isNew = [f.newEntry boolValue];
        if (isNew)
            count++;
    }
    
    return count;
}


@end
