//
//  iReadRSSTableCell.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 24/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface iReadRSSTableCell : NSTableCellView

@property (nonatomic, weak) IBOutlet NSTextField *description;
@property (nonatomic, weak) IBOutlet NSTextField *feed;
@property (nonatomic, weak) IBOutlet NSTextField *date;

-(void)doubleClick;
-(void)markItemAsRead:(NSNumber *)value;

@end
