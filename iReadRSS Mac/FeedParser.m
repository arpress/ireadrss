//
//  FeedParser.m
//  xmlparser
//
//  Created by Gian Marco Toso on 12/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import "FeedParser.h"
#import "NSString+Contains.h"

@implementation FeedParser

@synthesize stories;
@synthesize feedTitle;

- (id)initWithUrl:(NSURL *)theUrl
{
    self = [super init];
    if (self) {
        documentURL = theUrl;
        document = [[NSXMLDocument alloc] initWithContentsOfURL:documentURL options:NSDataReadingUncached error:NULL];
                    
        stories = [NSMutableArray array];
    }
    
    return self;
}

- (NSString *)stripTagsFromString:(NSString *)theString {
    NSMutableString *ms = [NSMutableString stringWithCapacity:[theString length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:theString];
    [scanner setCharactersToBeSkipped:nil];
    NSString *s = nil;
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&s];
        if (s != nil)
            [ms appendString:s];
        [scanner scanUpToString:@">" intoString:NULL];
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation]+1];
        s = nil;
    }
    
    return ms;
}

- (NSString *)reformat3339Date:(NSString *)date {
    NSArray *noZDate = [date componentsSeparatedByString:@"Z"];
    NSArray *splitDate = [noZDate[0] componentsSeparatedByString:@"T"];
    
    NSString *dateComponent = splitDate[0];
    NSArray *splitHour = [[splitDate lastObject] componentsSeparatedByString:@"."];
    
    NSString *timeComponent = splitHour[0];
    NSString *saneDate;
    if (splitHour.count > 1) {
        NSArray *timeZoneComponents = [[[splitHour lastObject] substringFromIndex:3] componentsSeparatedByString:@":"];
        
        saneDate = [NSString stringWithFormat:@"%@ %@ %@%@", dateComponent, timeComponent, timeZoneComponents[0], timeZoneComponents[1]];
    } else {
        saneDate = [NSString stringWithFormat:@"%@ %@ +0000", dateComponent, timeComponent];    
    }
        
    return saneDate;
}

- (NSString *)stringFromDate:(NSDate *)date {
    NSString *outputFormat = [NSString stringWithFormat:@"d LLL yyyy, HH:mm"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_us"];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:outputFormat];
    
    [dateFormatter setDateFormat:outputFormat];
    return [dateFormatter stringFromDate:date];
}

- (NSDate *)reformatDate:(NSString *)date {
    NSArray *dateComponents = [date componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    NSString *saneDate = dateComponents[0];
    NSString *theFormat = [NSString stringWithFormat:@"E, d LLL yyyy HH:mm:ss Z"];
    NSString *alternativeFormat = [NSString stringWithFormat:@"E, d LLL yyyy HH:mm:ss z"];
    NSLocale *usLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_us"];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setLocale:usLocale];
    [dateFormatter setDateFormat:theFormat];
    NSDate *theDate = [dateFormatter dateFromString:saneDate];
    
    if (!theDate) { 
        [dateFormatter setDateFormat:alternativeFormat];
        theDate = [dateFormatter dateFromString:saneDate];
    }
    
    if (!theDate) { // Maybe it's in the RFC3339 format...
        theDate = [NSDate dateWithString:[self reformat3339Date:saneDate]];
    }
    
    return theDate;
}

- (NSArray *)parseFeed {    
    NSXMLNode *root = [document rootElement];
    NSXMLNode *startingNode = root;
    BOOL isRSS = NO;
    
    if ([[[root.children lastObject] name] isEqualToString:@"channel"]) { // RSS
        isRSS = YES;
        startingNode = [root.children lastObject];  
    }
    
    for (NSXMLNode *currentNode in startingNode.children) {
        // IL TITOLO DEL FEED
        if ([[currentNode name] isEqualToString:@"title"])
            feedTitle = [currentNode stringValue];
            
        // IL FEED VERO E PROPRIO        
        if ([[currentNode name] isEqualToString:(isRSS) ? @"item" : @"entry"]) {
            NSArray *children = [currentNode children];
            NSMutableDictionary *currentStory = [NSMutableDictionary dictionary];
            BOOL linkFound = NO;
            BOOL summaryFound = NO;
            BOOL titleFound = NO;
            BOOL contentFound = NO;
            BOOL imageFound = NO;
            
            for (NSXMLElement *currentChild in children) {
                if ([[currentChild name] isEqualToString:@"title"]) {
                    currentStory[@"title"] = ([currentChild stringValue]) ? [currentChild stringValue] : @"";
                    titleFound = YES;
                }
                
                if (!summaryFound && [[currentChild name] isEqualToString:(isRSS) ? @"description" : @"summary"]) {
                    if ([currentChild stringValue] != nil) {
                        summaryFound = YES;
                        currentStory[@"description"] = [currentChild stringValue];
                    }
                }
                
                // FeedBurner enjoys disrespecting the standard, so here it is.
                if ([[currentChild name] isEqualToString:@"feedburner:origLink"] && !linkFound) {
                    currentStory[@"link"] = [currentChild stringValue];
                    linkFound = YES;
                }
                
                
                if (!linkFound && [[currentChild name] isEqualToString:@"link"]) {
                    // Second try: the link might be in the href attribute of the rel="alternate" link element. 
                    NSMutableDictionary *attributes = [NSMutableDictionary dictionary];
                    
                    for (NSXMLNode *attribute in [(NSXMLElement *)currentChild attributes]) {
                        attributes[[attribute name]] = [attribute stringValue];
                    }
                    
                    if ([attributes[@"rel"] isEqualToString:@"alternate"]) {
                        currentStory[@"link"] = attributes[@"href"];
                        linkFound = YES;
                    }
                    
                    // It also might be that the entry has only one link element, and the link is in its content.
                    if (!linkFound && ![[[currentChild stringValue] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] isEqualToString:@""]) {
                        currentStory[@"link"] = [currentChild stringValue];
                        linkFound = YES;
                    }
                }
                
                // ** ATOM ONLY **
                
                if ([[currentChild name] containsString:@"content"]) {
                    currentStory[@"feedContent"] = [currentChild stringValue];
                    contentFound = YES;
                    
                    if (!summaryFound) {
                        currentStory[@"description"] = [currentChild stringValue];
                        summaryFound = YES;
                    }
                }
                
                if ([currentChild.name isEqualToString:@"media:thumbnail"]) {
                    currentStory[@"imageURL"] = [[currentChild attributeForName:@"url"] stringValue];
                    imageFound = YES;
                }
                
                // ** ********* **
                
                if ([[currentChild name] isEqualToString:(isRSS) ? @"pubDate" : @"updated"]) {
                    NSDate *pubDate = [self reformatDate:[currentChild stringValue]];
                    if (!pubDate) pubDate = [NSDate date];
                    currentStory[@"pubDate"] = pubDate;
                    currentStory[@"internalDate"] = [self stringFromDate:pubDate];
                }
            }
            
            if (titleFound) {
                NSString *currentSummary;
                if (summaryFound) {
                    currentSummary = [(NSString *)currentStory[@"description"] gtm_stringByUnescapingFromHTML];
                    
                    // -- GAWKER FIX --
                    currentSummary = [self stripTagsFromString:currentSummary];
                    currentSummary = [currentSummary stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    
                    NSRegularExpression *expression = [NSRegularExpression regularExpressionWithPattern:@"^#[a-z0-9]+" options:NSRegularExpressionCaseInsensitive error:NULL];
                    
                    NSTextCheckingResult *match = [expression firstMatchInString:currentSummary options:0 range:NSMakeRange(0, [currentSummary length])];
                    
                    if (match.numberOfRanges != 0) {
                        currentSummary = [currentSummary substringFromIndex:match.range.length];
                        currentSummary = [currentSummary stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                    }
                    // -- END GAWKER FIX --
                    
                    if (currentSummary.length > 350) currentSummary = [currentSummary substringToIndex:350];
                }
                else {
                    currentSummary = [NSString string];
                }
                
                if (!linkFound)
                    currentStory[@"link"] = @"";
                
                if (!imageFound)
                    currentStory[@"imgeURL"] = @"";
                
                if (!contentFound) currentStory[@"feedContent"] = currentStory[@"description"];
                currentStory[@"description"] = currentSummary;
                
                [stories addObject:currentStory];
            }
        }        
    }    
    
    return nil;
}


@end
