//
//  CoreDataManager.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 25/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject


@property (nonatomic, strong, readonly) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (nonatomic, strong, readonly) NSManagedObjectModel *managedObjectModel;
@property (nonatomic, strong, readonly) NSManagedObjectContext *managedObjectContext;

- (IBAction)saveAction:(id)sender;
- (BOOL) saveData;
- (id) initWithPersistenStore: (NSPersistentStoreCoordinator *)coordinator;



@end
