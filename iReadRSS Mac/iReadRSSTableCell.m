//
//  iReadRSSTableCell.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 24/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import "iReadRSSTableCell.h"
#import "FeedItem.h"
#import "FeedSource.h"

@implementation iReadRSSTableCell

- (id)init {
    self = [super init];
    if (self) {

    }
    
    return self;
}

- (void)awakeFromNib {

}

- (void)drawRect:(NSRect)dirtyRect {
    FeedItem *cellObject = self.objectValue;
    
    if (cellObject.newEntry.boolValue == YES) {
        NSBezierPath *path = [NSBezierPath bezierPath];
        
        [path moveToPoint:CGPointMake(self.frame.size.width - 15, self.frame.size.height)];
        [path lineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height - 15)];
        [path lineToPoint:CGPointMake(self.frame.size.width, self.frame.size.height)];
        [path closePath];
        
        NSColor *deepBlue = [NSColor colorWithDeviceRed:60.0/256 green:119.0/256 blue:212.0/256 alpha:1];
        NSColor *lightBlue = [NSColor colorWithDeviceRed:94.0/256 green:203.0/256 blue:253.0/256 alpha:1];
        
        NSGradient *gradient = [[NSGradient alloc] initWithStartingColor:deepBlue endingColor:lightBlue];
        
        [path setFlatness:1];
        [path setLineWidth:2];
        [deepBlue set];
        [path stroke];
        
        
        [gradient drawInBezierPath:path angle:45];
    }   // Draws unread item bevel
    
    if (cellObject.readLater.boolValue == YES) {        
        NSBezierPath *path = [NSBezierPath bezierPath];
        
        [path moveToPoint:CGPointMake(self.frame.size.width - 15, self.frame.origin.y + 2)];
        [path lineToPoint:CGPointMake(self.frame.size.width, self.frame.origin.y + 2)];
        [path lineToPoint:CGPointMake(self.frame.size.width, self.frame.origin.y + 17)];
        [path closePath];
        
        NSColor *deepBlue = [NSColor colorWithDeviceRed:160.0/256 green:219.0/256 blue:12.0/256 alpha:1];
        NSColor *lightBlue = [NSColor colorWithDeviceRed:194.0/256 green:213.0/256 blue:53.0/256 alpha:1];
        
        NSGradient *gradient = [[NSGradient alloc] initWithStartingColor:deepBlue endingColor:lightBlue];
        
        [path setFlatness:1];
        [path setLineWidth:2];
        [deepBlue set];
        [path stroke];
        
        
        [gradient drawInBezierPath:path angle:45];
    }
    
    NSBezierPath *bottomBorder = [NSBezierPath bezierPath];
    [bottomBorder moveToPoint:CGPointMake(0, 0)];
    [bottomBorder lineToPoint:CGPointMake(self.frame.size.width, 0)];
    
    [[NSColor lightGrayColor] set];
    [bottomBorder stroke];
}

- (void)doubleClick {
    FeedItem *cellObject = self.objectValue;
    
    if ([cellObject.source.shouldAutoDownload boolValue]) {
        NSString *filePath = [cellObject.source.downloadPath stringByAppendingPathComponent:cellObject.downloadFile];
        
        [[NSWorkspace sharedWorkspace] openFile:filePath];
        return;
    }
    
    NSString *link = cellObject.link;
    NSString *saneLink = [link stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
    
    NSURL *toOpen = [NSURL URLWithString:saneLink];
    if (![[NSWorkspace sharedWorkspace] openURL:toOpen])
        NSLog(@"Unable to open link %@", saneLink);
    
    [self markItemAsRead:[NSNumber numberWithBool:YES]];
}

- (void)markItemAsRead:(NSNumber *)value {
    FeedItem *cellObject = self.objectValue;
    
    [cellObject markAsRead:value];
    self.needsDisplay = YES;
}

- (void)bookmarkItem {
    FeedItem *cellObject = self.objectValue;
    
    [cellObject bookmarkItem];
    self.needsDisplay = YES;
}

@end
