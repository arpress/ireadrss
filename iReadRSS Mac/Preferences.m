//
//  Preferences.m
//  RSSReader
//
//  Created by Gian Marco Toso on 11/28/11.
//  Copyright 2011 None. All rights reserved.
//

#import "Preferences.h"

@implementation Preferences

@synthesize fetchInterval, deleteItemsOlderThan, showBadgeOnIcon, bounceDockIconOnNewItems, isDefaultReader, useGrowl, groupGrowlNotifications, groupSources, disableDlsNotifications, shouldCacheImages, playSoundOnNewItems;

-(id)init{
    self = [super init];
    if (self) {
        [self readPlist];
        return self;
    }
    
    return nil;
}

-(void)fillPreferences:(NSDictionary *)contents {
    NSNumber *_fetchInterval = (NSNumber *)contents[@"fetchInterval"];
    NSNumber *_deleteItemsOlderThan = (NSNumber *)contents[@"deleteItemsOlderThan"];
    NSNumber *_showBadgeIcon = (NSNumber *)contents[@"showBadgeOnIcon"];
    NSNumber *_bounceDockIcon = (NSNumber *)contents[@"bounceDockIconOnNewItems"];
    NSNumber *_isDefaultReader = (NSNumber *)contents[@"isDefaultReader"];
    NSNumber *_useGrowl = (NSNumber *)contents[@"useGrowl"];
    NSNumber *_groupGrowlNotifications = (NSNumber *)contents[@"groupGrowlNotifications"];
    NSNumber *_groupSources = (NSNumber *)contents[@"groupSources"];
    NSNumber *_disableDlsNotifications = (NSNumber *)contents[@"disableDownloadNotifications"];
    NSNumber *_shouldCacheImages = (NSNumber *)contents[@"shouldCacheImages"];
    NSNumber *_playSoundOnNewItems = (NSNumber *)contents[@"playSoundOnNewItems"];


    
    self.fetchInterval = (_fetchInterval != nil) ? _fetchInterval : DEFAULT_FETCHINTERVAL;
    self.deleteItemsOlderThan = (_deleteItemsOlderThan != nil) ? _deleteItemsOlderThan : DEFAULT_DELETEITEMSOLDERTHAN;
    self.showBadgeOnIcon = (_showBadgeIcon != nil) ? _showBadgeIcon : DEFAULT_SHOWBADGEONICON;
    self.bounceDockIconOnNewItems = (_bounceDockIcon != nil) ? _bounceDockIcon : DEFAULT_BOUNCEDOCKICON;
    self.isDefaultReader = (_isDefaultReader != nil) ? _isDefaultReader : DEFAULT_ISDEFAULTREADER;
    self.useGrowl = (_useGrowl != nil) ? _useGrowl : DEFAULT_USEGROWL;
    self.groupGrowlNotifications = (_groupGrowlNotifications != nil) ? _groupGrowlNotifications : DEFAULT_GROUPGROWLNOTIFICATIONS;
    self.groupSources = (_groupSources != nil) ? _groupSources : DEFAULT_GROUPSOURCES;
    self.disableDlsNotifications = (_disableDlsNotifications != nil) ? _disableDlsNotifications : DEFAULT_DISABLEDLSNOTIFICATIONS;
    self.shouldCacheImages = (_shouldCacheImages != nil) ? _shouldCacheImages : DEFAULT_SHOULDCACHEIMAGES;
    self.playSoundOnNewItems = (_playSoundOnNewItems != nil) ? _playSoundOnNewItems : DEFAULT_PLAYSOUNONNEWITEMS;
}

-(void)readPlist {
    NSString *errorDesc = nil;
    NSPropertyListFormat format;
    NSString *plistPath;
    NSString *rootPath = [[NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory,
                                                              NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"iReadRSS/"];
    plistPath = [rootPath stringByAppendingPathComponent:@"Preferences.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath]) {
        plistPath = [[NSBundle mainBundle] pathForResource:@"Preferences" ofType:@"plist"];
    }
    
    NSData *plistXML = [[NSFileManager defaultManager] contentsAtPath:plistPath];
    NSDictionary *temp = (NSDictionary *)[NSPropertyListSerialization
                                          propertyListFromData:plistXML
                                          mutabilityOption:NSPropertyListMutableContainersAndLeaves
                                          format:&format
                                          errorDescription:&errorDesc];
    if (!temp) {
        NSLog(@"Error reading plist: %@, format: %lu", errorDesc, format);
    }
    
    [self fillPreferences:temp];
}

-(BOOL)savePlist {
    NSString *plistError;
    NSString *rootPath = [[NSSearchPathForDirectoriesInDomains(NSApplicationSupportDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"iReadRSS/"];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:@"Preferences.plist"];
    
    BOOL isDir = NO;
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:rootPath isDirectory:&isDir]) {
        NSError *error;
        
        [[NSFileManager defaultManager] createDirectoryAtPath:rootPath withIntermediateDirectories:YES attributes:nil error:&error];
        
        if (error != nil) {
            NSLog(@"Unable to create application directory in user's library: %@", error);
            return NO;
        }
    }
    
    NSDictionary *plistDict = [NSDictionary dictionaryWithObjects:
                               @[fetchInterval, 
                                deleteItemsOlderThan,
                                showBadgeOnIcon, 
                                bounceDockIconOnNewItems,
                                isDefaultReader,
                                useGrowl,
                                groupGrowlNotifications,
                                groupSources,
                                disableDlsNotifications,
                                shouldCacheImages,
                                playSoundOnNewItems ]
                               
                                forKeys:@[@"fetchInterval", 
                                         @"deleteItemsOlderThan", 
                                         @"showBadgeOnIcon", 
                                         @"bounceDockIconOnNewItems",
                                         @"isDefaultReader",
                                         @"useGrowl",
                                         @"groupGrowlNotifications",
                                         @"groupSources",
                                         @"disableDownloadNotifications",
                                         @"shouldCacheImages",
                                         @"playSoundOnNewItems"]];
    
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:plistDict
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&plistError];
    if(!plistData) {
        NSLog(@"%@", plistError);
        return  NO;        
    }
    
    //[plistData writeToFile:plistPath atomically:YES];
    
    NSError *error;
    [plistData writeToFile:plistPath options:NSDataWritingAtomic error:&error];
    
    if (error != nil) {
        NSLog(@"Unable to save preferences: %@", error);
        
        return NO;
    }
    
    return YES;
}

-(void)awakeFromNib {
    [self readPlist];
}

@end
