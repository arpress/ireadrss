//
//  iReadRSSAppDelegate.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 24/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "iReadRSSTableCell.h"
#import "iReadRSSFeedModel.h"
#import "SettingsWindowController.h"
#import "Preferences.h"
#import "CoreDataManager.h"

@interface iReadRSSAppDelegate : NSObject <NSApplicationDelegate, NSTableViewDelegate, NSTableViewDataSource> {
    
    NSTimer *fetchTimer;
    
    NSPredicate *sourceFilter;
    NSPredicate *readLaterFilter;
    NSPredicate *customFilter;
    
    BOOL parseIsRunning;
}

@property (assign) IBOutlet NSWindow *window;
@property (weak) IBOutlet NSScrollView *scrollView;
@property (weak) IBOutlet NSTableView *tableView;
@property (weak) IBOutlet NSTableView *downloadHistory;
@property (nonatomic, strong) iReadRSSFeedModel *model;
@property (nonatomic, weak) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) SettingsWindowController *settingsWindowController;
@property (weak) IBOutlet NSTextField *updateText;
@property (weak) IBOutlet NSProgressIndicator *updateIndicator;
@property (weak) IBOutlet NSArrayController *unreadItems;
@property (weak) IBOutlet NSPopUpButton *filterMenu;

/* Add Feed Modal Sheet */
@property (weak) IBOutlet NSTextField *addFeedURLField;
@property (weak) IBOutlet NSTextField *addFeedNameField;
@property (unsafe_unretained) IBOutlet NSWindow *addFeedWindow;
@property (weak) IBOutlet NSButton *addFeedDownloadCheckbox;
@property (weak) IBOutlet NSBox *addFeedDownloadBox;
@property (weak) IBOutlet CustomPathControl *addFeedDownloadPath;

@property (nonatomic, strong) Preferences *prefs;
@property (strong) CoreDataManager *dataManager;
@property (strong) NSPredicate *filterPredicateArray;
@property (weak) IBOutlet NSArrayController *itemsArrayController;
@property (weak) IBOutlet NSArrayController *unreadItemsArrayController;

@property (nonatomic, strong) NSArray *feedSortDescriptors;
@property (weak) IBOutlet NSPopover *popover;
@property (weak) IBOutlet NSButton *showDownloadHistoryButton;
@property (nonatomic) BOOL newFeedIsDownload;

- (IBAction)filterSource:(id)sender;
- (IBAction)filterCustomSearch:(id)sender;
- (IBAction)filterBookmarks:(NSButton *)sender;

- (IBAction)openPreferencesWindow:(id)sender;
- (IBAction)addFeedModalWindowButtonClick:(id)sender;
- (IBAction)runParse:(id)sender;
- (IBAction)markAllItemsAsRead:(id)sender;
- (IBAction)showPopOver:(NSButton *)sender;

- (IBAction)markItemAsRead:(id)sender;
- (IBAction)markItemAsUnread:(id)sender;
- (IBAction)openSelectedItem:(id)sender;
- (IBAction)bookmarkItem:(id)sender;

- (IBAction)checkAddFeedDownloadCheckbox:(id)sender;

- (BOOL)tableViewIsSelected;
- (BOOL)downloadHistoryIsSelected;

@end
