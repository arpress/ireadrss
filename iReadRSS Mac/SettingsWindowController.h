//
//  SettingsWindowController.h
//  RSSReader
//
//  Created by Gian Marco Toso on 11/15/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>

#include "CoreDataManager.h"
#include "FeedSource.h"
#include "FeedItem.h"
#include "CustomPathControl.h"
#include "Preferences.h"

@interface SettingsWindowController : NSWindowController <NSTableViewDelegate> {
    NSView *theView;
    
    NSManagedObjectContext *__weak managedObjectContext;
    NSArrayController *__weak sourcesArray;
    NSArrayController *__weak itemsArray;
    NSTableView *__weak sourcesTable;
    NSTextField *__weak fetchIntervalField;
    NSTextField *__weak deleteItemsOlderThanField;
    NSButton *__weak showBadgeOnIconCheckbox;
    NSButton *__weak bounceDockIconOnNewItemsCheckbox;
    NSBox *__weak generalSettingsBox;
    NSBox *__weak sourcesBox;
    NSBox *__weak notificationsBox;
    NSBox *__weak downloadsBox;
    NSToolbarItem *__weak generalSettingsToolbarItem;
    NSToolbar *__weak toolbar;
    CustomPathControl *__weak pathSelector;
    NSWindow *__weak addFeedWindow;
    NSTextField *newFeedSourceURL;
    NSTextField *newFeedSourceName;
    Preferences *__weak preferences;
    NSPathControl *pathControl;


}


@property (strong) IBOutlet NSView *theView;
@property (weak, readonly) NSManagedObjectContext *managedObjectContext;
@property (weak) IBOutlet NSArrayController *sourcesArray;
@property (weak) IBOutlet NSArrayController *itemsArray;
@property (weak) IBOutlet NSTableView *sourcesTable;
@property (weak) IBOutlet NSTextField *fetchIntervalField;
@property (weak) IBOutlet NSTextField *deleteItemsOlderThanField;
@property (weak) IBOutlet NSButton *showBadgeOnIconCheckbox;
@property (weak) IBOutlet NSButton *bounceDockIconOnNewItemsCheckbox;
@property (weak) IBOutlet NSBox *generalSettingsBox;
@property (weak) IBOutlet NSBox *sourcesBox;
@property (weak) IBOutlet NSBox *notificationsBox;
@property (weak) IBOutlet NSBox *downloadsBox;
@property (weak) IBOutlet NSToolbarItem *generalSettingsToolbarItem;
@property (weak) IBOutlet NSToolbar *toolbar;
@property (weak) IBOutlet CustomPathControl *pathSelector;
@property (weak) IBOutlet NSWindow *addFeedWindow;
@property (weak) IBOutlet NSTextField *addedFeedSourceURL;
@property (weak) IBOutlet NSTextField *addedFeedSourceName;
@property (weak) IBOutlet Preferences *preferences;

- (IBAction)addFeedModalWindowButtonClick:(id)sender;
- (IBAction)addButtonClicked:(id)sender;
- (IBAction)deleteButtonClicked:(id)sender;
- (IBAction)toolbarButtonClick:(id)sender;
- (IBAction)testButtonClick:(id)sender;

- (BOOL) saveData;
- (id)initInManagedContext:(NSManagedObjectContext *)theContext;
- (void)updateDownloadPathForCurrentSource:(NSNotification *)notification;

@end
