//
//  FeedParser.h
//  xmlparser
//
//  Created by Gian Marco Toso on 12/13/11.
//  Copyright 2011 None. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GTMNSString+HTML.h"

@interface FeedParser : NSObject {
    NSURL *documentURL;
    NSXMLDocument *document;
    
    NSMutableArray *stories;
    NSString *feedTitle;
}

@property (strong, nonatomic) NSMutableArray *stories;
@property (strong) NSString *feedTitle;

- (id)initWithUrl:(NSURL *)theUrl;
- (NSArray *)parseFeed;
- (NSString *)stripTagsFromString:(NSString *)theString;

@end
