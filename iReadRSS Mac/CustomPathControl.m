//
//  CustomPathControl.m
//  RSSReader
//
//  Created by Gian Marco Toso on 11/30/11.
//  Copyright 2011 None. All rights reserved.
//

#import "CustomPathControl.h"

@implementation CustomPathControl

@synthesize currentURL;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (void)selectItemAtIndex:(NSInteger)index {
    [super selectItemAtIndex:index];
    
    self.currentURL = [NSURL URLWithString:[self.selectedItem title]];
}

- (void)setEnabled:(BOOL)flag {
    [super setEnabled:flag];
    [self updateContent:[currentURL relativePath]];
}

- (void)postNewPathNotification {
    NSDictionary *args = @{@"path": currentURL};
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CustomPathControlItemSelected" object:self userInfo:args];
}

- (void)updateContent:(NSString *)itemURL {
    BOOL shouldRepost = NO;
    
    if ([itemURL isEqualToString:@"UserDownloads"]) {
        itemURL = [NSString stringWithString:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"]];
        shouldRepost = YES;
    }
    
    self.currentURL = [NSURL URLWithString:itemURL];
    [self selectItem:[self menuItemForURL:([self isEnabled] ? currentURL : nil)]];
    
    if (shouldRepost) [self postNewPathNotification];
}

- (void)menuItemAction:(id)sender {
    self.currentURL = [NSURL URLWithString:[sender title]];
    
    [self postNewPathNotification];
}

- (NSMenuItem *)menuItemForURL:(NSURL *)theURL {
    NSMenu *menu = [self menu];
    NSMenuItem *theItem;
    NSString *relativePath = [theURL relativePath];
    
    if (relativePath == nil) 
        return nil;
    
    if ((theItem = [menu itemWithTitle:relativePath]) != nil)
        return theItem;
    
    theItem = [[NSMenuItem allocWithZone:[NSMenu menuZone]] initWithTitle:relativePath action:@selector(menuItemAction:) keyEquivalent:@""];
	[theItem setTarget:self];
    [self setStringValue:[theURL relativePath]];
    
	NSImage* menuItemIcon = [[NSWorkspace sharedWorkspace] iconForFile:relativePath];
	[menuItemIcon setSize:NSMakeSize(16, 16)];
	[theItem setImage:menuItemIcon];
	[menu addItem:theItem];
	return theItem;
}

- (void)chooseDirectory:(id)sender {
    NSOpenPanel *openPanel = [NSOpenPanel openPanel];
    
	[openPanel setAllowsMultipleSelection:NO];
	[openPanel setCanChooseDirectories:YES];
	[openPanel setCanChooseFiles:NO];
    [openPanel setCanCreateDirectories:YES];
	[openPanel setResolvesAliases:YES];
	[openPanel setTitle:@"Choose where to download the files for this feed"];
	[openPanel setPrompt:@"Ok"];
    
    [openPanel beginSheetModalForWindow:[self window] completionHandler:^(NSInteger returnCode) {
        if (returnCode == NSOKButton) {
            self.currentURL = [NSURL URLWithString:[[openPanel URLs][0] relativePath]];
            
            if (self.itemArray.count == 4)
                [self.menu addItem:[NSMenuItem separatorItem]];
        }
        
        [self selectItem:[self menuItemForURL:(currentURL)]];        
        [self postNewPathNotification];
    }];
}

- (void)initialize {
    NSMenu *menu = self.menu;
    NSMenuItem *choose = [[NSMenuItem allocWithZone:[NSMenu menuZone]] initWithTitle:@"Choose..." action:@selector(chooseDirectory:) keyEquivalent:@""];
    [choose setTarget:self];
    
    [menu addItem:choose];
	[menu addItem:[NSMenuItem separatorItem]];  
    
    [self menuItemForURL:[NSURL URLWithString:NSHomeDirectory()]];
    [self menuItemForURL:[NSURL URLWithString:[NSHomeDirectory() stringByAppendingPathComponent:@"Downloads"]]];    
}

- (void)awakeFromNib {
    [self initialize];
}

@end
