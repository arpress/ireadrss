//
//  CustomPathControl.h
//  RSSReader
//
//  Created by Gian Marco Toso on 11/30/11.
//  Copyright 2011 None. All rights reserved.
//

@interface CustomPathControl : NSPopUpButton {
    NSURL *currentURL;
}

@property (strong) NSURL *currentURL;

- (NSMenuItem *)menuItemForURL:(NSURL *)theURL;
- (void)updateContent:(NSString *)itemURL;

@end
