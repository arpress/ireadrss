//
//  NSObject+NSString_Contains.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 1/20/13.
//  Copyright (c) 2013 Gian Marco Toso. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSString_Contains)

- (BOOL)containsString:(NSString *)string;
- (BOOL)containsString:(NSString *)string
               options:(NSStringCompareOptions) options;
- (BOOL)containedStringLocation: (NSString *) string
                        options:(NSStringCompareOptions)options;


@end
