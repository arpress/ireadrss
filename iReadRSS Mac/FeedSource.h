//
//  FeedSource.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 25/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FeedItem;

@interface FeedSource : NSManagedObject

@property (nonatomic, retain) NSNumber * autoOpen;
@property (nonatomic, retain) NSNumber * autoRead;
@property (nonatomic, retain) NSString * downloadPath;
@property (nonatomic, retain) NSNumber * justDownload;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * shouldAutoDownload;
@property (nonatomic, retain) NSString * status;
@property (nonatomic, retain) NSString * url;
@property (nonatomic, retain) NSSet *items;

@end

@interface FeedSource (CoreDataGeneratedAccessors)

+ (FeedSource *) feedSourceWithName:(NSString *)name andURL:(NSString *)theURL inContext:(NSManagedObjectContext *)context;
+ (FeedSource *) feedSourceWithName: (NSString *)name inContext:(NSManagedObjectContext *)context;

- (NSUInteger)unreadItems;

- (void)addItemsObject:(FeedItem *)value;
- (void)removeItemsObject:(FeedItem *)value;
- (void)addItems:(NSSet *)values;
- (void)removeItems:(NSSet *)values;

@end
