//
//  SettingsWindowController.m
//  RSSReader
//
//  Created by Gian Marco Toso on 11/15/11.
//  Copyright 2011 None. All rights reserved.
//

#import "SettingsWindowController.h"

@implementation SettingsWindowController
@synthesize theView;
@synthesize managedObjectContext;
@synthesize sourcesArray;
@synthesize itemsArray;
@synthesize sourcesTable;
@synthesize fetchIntervalField;
@synthesize deleteItemsOlderThanField;
@synthesize showBadgeOnIconCheckbox;
@synthesize bounceDockIconOnNewItemsCheckbox;
@synthesize generalSettingsBox;
@synthesize sourcesBox;
@synthesize notificationsBox;
@synthesize downloadsBox;
@synthesize generalSettingsToolbarItem;
@synthesize toolbar;
@synthesize pathSelector;
@synthesize addFeedWindow;
@synthesize addedFeedSourceURL;
@synthesize addedFeedSourceName;
@synthesize preferences;

- (id)initInManagedContext:(NSManagedObjectContext *)theContext
{
    self = [super initWithWindowNibName:@"SettingsWindow"];
    if (self) {
        managedObjectContext = theContext;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateDownloadPathForCurrentSource:) name:@"CustomPathControlItemSelected" object:pathSelector];
    }
    
    return self;
}

- (void)updateDownloadPathForCurrentSource:(NSNotification *)notification {
    NSURL *newPath = [notification userInfo][@"path"];

    [(FeedSource *)[[sourcesArray selectedObjects] lastObject] setDownloadPath:[newPath relativePath]];
}

- (void)tableViewSelectionDidChange:(NSNotification *)notification {
    [pathSelector updateContent:[(FeedSource *)[[sourcesArray selectedObjects] lastObject] downloadPath]];
}

- (void)resizeWindowToHeight:(float)newHeight animate:(BOOL)animate {
    float delta = [[self window] frame].size.height - newHeight - 85;
    NSRect newFrame = [[self window] frame];
    
    newFrame.origin.y += delta;
    newFrame.size.height = newHeight + 85;    
    
    [[self window] setFrame:newFrame display:YES animate:animate];
}

- (void)addBoxToMainView:(NSBox *)theBox {
    [[self theView] addSubview:theBox];
    [theBox setFrame:NSMakeRect(0, [self theView].frame.size.height - theBox.frame.size.height, theBox.frame.size.width, theBox.frame.size.height)];
}

- (void) awakeFromNib {    
    NSArray *boxes = @[generalSettingsBox, sourcesBox, notificationsBox, downloadsBox];
    
    for (NSBox *box in boxes) {
        [self addBoxToMainView:box];
        [box setHidden:YES];
    }
    
    [generalSettingsBox setHidden:NO];
    [self resizeWindowToHeight:generalSettingsBox.frame.size.height animate:NO];
    
    [toolbar setSelectedItemIdentifier:[generalSettingsToolbarItem itemIdentifier]];
    
    [sourcesTable setRowHeight:30.0];
}

-(BOOL)savePlist {
    return [self.preferences savePlist];
}

- (IBAction)toolbarButtonClick:(id)sender {
    NSArray *boxes = @[generalSettingsBox, sourcesBox, notificationsBox, downloadsBox];
    float newHeight = 0;
    
    for (NSBox *box in boxes) {
        if ([[box title] isEqualToString:[(NSToolbarItem*)sender label]]) {            
            newHeight = box.frame.size.height;
            [box setHidden:NO];
        } else {
            [box setHidden:YES];
        }
    }
    
    [self resizeWindowToHeight:newHeight animate:YES];
}

- (NSURL *)handleFeedSchema:(NSString *)url {
    NSArray *stringComponents = [url componentsSeparatedByString:@"feed://"];
    
    if (stringComponents.count == 1) {
        return [NSURL URLWithString:url];
    } else {
        NSString *basePath = (NSString *)CFSTR("http://");
        NSURL *toReturn = [NSURL URLWithString:[basePath stringByAppendingString:[stringComponents lastObject]]];
        return toReturn;  
    }
}

- (IBAction)testButtonClick:(id)sender {
    for (FeedSource *source in [sourcesArray arrangedObjects]) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[self handleFeedSchema:source.url] cachePolicy:NSURLCacheStorageNotAllowed timeoutInterval:30];
        NSURLResponse *response = nil;
        NSError *error = nil;
        
        [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
        
        if (error != nil) {
            source.status = @"Error";
        } else {
            source.status = @"Ok";
        }
    }
}

- (BOOL) saveData {
    NSError *error = nil;
    
    if (![managedObjectContext hasChanges])
        return NO;
    
    if (![managedObjectContext commitEditing]) {
        NSLog(@"%@:%@ unable to commit editing before saving", [self class], NSStringFromSelector(_cmd));
        return NO;
    }
    
    if (![managedObjectContext save:&error]) {
        [[NSApplication sharedApplication] presentError:error];
        return NO;
    }    
    
    return YES;
}

- (IBAction)addFeedModalWindowButtonClick:(id)sender {
    if ([sender tag] == 1) {
        [NSApp endSheet:addFeedWindow];
        [addFeedWindow orderOut:self];
        return;
    }
    
    NSString *newSourceURL = [addedFeedSourceURL stringValue];
    NSString *newSourceName = [addedFeedSourceName stringValue];
    
    if (newSourceURL == nil || [[newSourceURL stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        return;
    
    if (newSourceName == nil || [[newSourceName stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        return;
    
    [FeedSource feedSourceWithName:newSourceName andURL:newSourceURL inContext:[self managedObjectContext]];
    [NSApp endSheet:addFeedWindow];
    [addFeedWindow orderOut:self];
}

- (void)addSheetClosed {
    [sourcesArray fetch:self];
    [newFeedSourceURL setStringValue:@""];
    [newFeedSourceName setStringValue:@""];    
}

- (IBAction)addButtonClicked:(id)sender {
    [NSApp beginSheet:addFeedWindow modalForWindow:[self window] modalDelegate:self didEndSelector:@selector(addSheetClosed) contextInfo:nil];
}

- (void) deleteAlertClosed:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == NSAlertAlternateReturn) {
        FeedSource *selected = [[sourcesArray selectedObjects] lastObject];
        
        if (selected == nil) 
            return;
        
        NSArray *items = [[selected items] sortedArrayUsingDescriptors:nil];
        [itemsArray removeObjects:items];
        [sourcesArray performSelector:@selector(remove:)];
    }
}

- (IBAction)deleteButtonClicked:(id)sender {
    NSAlert *deleteAlert = [NSAlert alertWithMessageText:@"Do you really want to remove the selected source?" defaultButton:@"No" alternateButton:@"Yes" otherButton:nil informativeTextWithFormat:@"By removing the source, you'll also delete all the its stored news. The downloaded files will not be deleted."];
    
    [deleteAlert setIcon:[NSImage imageNamed:NSImageNameCaution]];
    
    [deleteAlert beginSheetModalForWindow:[self window] modalDelegate:self didEndSelector:@selector(deleteAlertClosed:returnCode:contextInfo:) contextInfo:nil];
}
 
- (BOOL) windowShouldClose:(id)sender {    
    [self savePlist];
    [self saveData];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsWindowHasClosed" object:nil];
    
    return YES;
}

@end
