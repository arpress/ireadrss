//
//  FeedItem.h
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 25/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FeedSource;

@interface FeedItem : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSDate * downloadDate;
@property (nonatomic, retain) NSString * downloadFile;
@property (nonatomic, retain) NSData * image;
@property (nonatomic, retain) NSString * link;
@property (nonatomic, retain) NSNumber * newEntry;
@property (nonatomic, retain) NSString * readableDate;
@property (nonatomic, retain) NSNumber * readLater;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * feedContent;
@property (nonatomic, retain) NSString * imageURL;
@property (nonatomic, retain) FeedSource *source;

@property (nonatomic, strong) NSImage * itemImage;

+ (FeedItem *) findFeedItemWithData:(NSDictionary *)data inContext:(NSManagedObjectContext *)context;
+ (FeedItem *) feedItemWithData: (NSDictionary *)data fromSource: (NSString *)sourceName inContext:(NSManagedObjectContext *)context shouldSkipSearch:(BOOL)skip notOlderThan:(NSUInteger)days;

+ (NSArray *) getAllItemsInContext:(NSManagedObjectContext *)context;

+ (NSArray *) getItemsOlderThan:(NSUInteger)days inContext:(NSManagedObjectContext *)context;

- (void)markAsRead:(NSNumber *)value;
- (void)bookmarkItem;

@end
