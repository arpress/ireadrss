iReadRSS
================

## Description

A simple RSS app with a nice auto-download feature as an added bonus. It downloads your favourite feeds and shows them to you in a Twitter-like interface, using your default to browser to open the links (I never saw why one should read webpages somewhere else). You can set the app to be your default RSS schema handler, so it will automatically add feeds when you try to open one, and you can also tell it to automatically download a feed's linked file (useful if it doesn't point to web pages) and open it, if you want to.
There is still some stuff to do, some errors to correct, some features to implement and most likely some bugs to squash, but it's here for you to download, use and modify however you like.

If you want the compiled and ready to use version you can find it here: http://www.gianmarcotoso.com/projects/ireadrss

Requires at least OSX 10.7! 

## A word of warning

This code might be very rough around the edges, since I did it to learn Objective-C and the Cocoa framework so I did not really know neither syntax nor classes when I begun. Also, some files are scattered here and there because, well, XCode decided that way and I still need to do housecleaning. The app itself seems to be stable, but I reckon there may be some bugs that could cause it to crash - if you happen upon one, do tell! :)

I'm very open to suggestions on how to clean up the code, which features to implement next and anything you can think of :)

Enjoy and code on!


