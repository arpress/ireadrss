//
//  iReadRSSFeedModel.m
//  iReadRSS Mac
//
//  Created by Gian Marco Toso on 24/10/2012.
//  Copyright (c) 2012 Gian Marco Toso. All rights reserved.
//

#import "iReadRSSFeedModel.h"
#import "iReadRSSAppDelegate.h"
#import "Preferences.h"

@interface iReadRSSFeedModel()

@property (nonatomic, weak) CoreDataManager *manager;
@property (nonatomic, strong) Preferences *prefs;

@end

@implementation iReadRSSFeedModel

@synthesize newsCollection = _newsCollection;
@synthesize manager = _manager;
@synthesize prefs = _prefs;

- (Preferences *)prefs {
    if (!_prefs) {
        _prefs = [[Preferences alloc] init];
    }
    
    [_prefs readPlist];
    
    return _prefs;
}

- (iReadRSSFeedModel *)initWithCoreDataManager:(CoreDataManager *)manager {
    self = [self init];
    
    if (self) {
        self.manager = manager;
    }
    
    return self;
}


- (NSArray *)newsCollection {
    if (!_newsCollection)
        _newsCollection = [NSArray array];
    
    return _newsCollection;
}

- (void) downloadFeedLink:(FeedItem *)feedItem {
    if (feedItem.link == nil || [[feedItem.link stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        return;
    
    NSString *downloadPath = feedItem.source.downloadPath;
    
    if (downloadPath == nil || [[downloadPath stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]] length] == 0)
        return;
    
    NSError *error = nil;
    NSURLRequest *theRequest = [NSURLRequest requestWithURL:[NSURL URLWithString:feedItem.link]];
    NSURLResponse *theResponse = nil;
    NSData *fileContent = [NSURLConnection sendSynchronousRequest:theRequest returningResponse:&theResponse error:&error];
    
    if (error != nil) {
        NSLog(@"There was an error downloading the file for %@", feedItem.title);
        return;
    }
    
    NSString *fileName = [theResponse suggestedFilename];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    if (![fileManager createFileAtPath:[downloadPath stringByAppendingPathComponent:fileName] contents:fileContent attributes:nil]) {
        NSLog(@"Unable to create or write to %@/%@ , please check user permissions.", downloadPath, fileName);
        return;
    }
    
    feedItem.downloadFile = fileName;
    feedItem.downloadDate = [NSDate date];
    
    if ([feedItem.source.autoOpen boolValue]) {
        [[NSWorkspace sharedWorkspace] openFile:[downloadPath stringByAppendingPathComponent:fileName]];
    }
    
    if (self.prefs.useGrowl.boolValue && !self.prefs.disableDlsNotifications.boolValue) {
        NSString *message = [NSString stringWithFormat:@"Downloaded: %@", fileName];
        
        [GrowlApplicationBridge notifyWithTitle:[NSString stringWithFormat:@"%@", feedItem.source.name]
                                    description:message
                               notificationName:@"NewDownload"
                                       iconData:nil
                                       priority:0
                                       isSticky:NO
                                   clickContext:nil];
    }
}


- (NSURL *)handleFeedSchema:(NSString *)url {
    NSArray *stringComponents = [url componentsSeparatedByString:@"feed://"];
    
    if (stringComponents.count == 1) {
        return [NSURL URLWithString:url];
    } else {
        NSString *basePath = (NSString *)CFSTR("http://");
        NSURL *toReturn = [NSURL URLWithString:[basePath stringByAppendingString:[stringComponents lastObject]]];
        return toReturn;
    }
}

- (void)parseFeeds {
    NSDictionary *threadArgs = @{@"coordinator": self.manager.persistentStoreCoordinator};
    
    NSThread *parseThread = [[NSThread alloc] initWithTarget:self selector:@selector(parseFeedsThreadCallback:) object:threadArgs];
    
    [parseThread start];
}

- (void)parseFeedsThreadCallback:(NSDictionary *)args {
    // Since the NSManagedObjectContext is not thread safe, I create another one, wrapped in a CoreDataManager, from the NSPersistenStoreCoordinator - which is thread safe!
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedParseBegin" object:self userInfo:nil];
    
    NSPersistentStoreCoordinator *coordinator = args[@"coordinator"];
    CoreDataManager *localCoreDataManager = [[CoreDataManager alloc] initWithPersistenStore:coordinator];
    
    // Changes need to be merged on the main NSManagedObjectContext when this one is done
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(mergeChanges:) name:NSManagedObjectContextDidSaveNotification object:localCoreDataManager.managedObjectContext];
    
    // Fetch all sources from the database
    NSFetchRequest *fetch = [[NSFetchRequest alloc] init];
    NSError *error = nil;
    NSArray *feedSources = nil;
    
    fetch.entity = [NSEntityDescription entityForName:@"FeedSource" inManagedObjectContext:localCoreDataManager.managedObjectContext];
    fetch.predicate = nil;  // ALL objects
    feedSources = [localCoreDataManager.managedObjectContext executeFetchRequest:fetch error:&error];
    
    // Fetch all news from all sources
    int totalCount = 0;
    
    for (FeedSource *currentSource in feedSources) {
        // Fetch news from this source
        NSArray *fetchedNews = [NSArray array];
        NSURL *currentURL = [self handleFeedSchema:currentSource.url];
        int skipped = 0;
        
        FeedParser *parser = [[FeedParser alloc] initWithUrl:currentURL];
        [parser parseFeed];
        
        // Save all news in an array
        fetchedNews = [parser.stories copy];
        
        // Only add those that are not already in the database
        for(NSDictionary *story in fetchedNews) {
            if (![FeedItem findFeedItemWithData:story inContext:localCoreDataManager.managedObjectContext]) {
                
                FeedItem *thisOne = [FeedItem feedItemWithData:story
                                                    fromSource:currentSource.name
                                                     inContext:localCoreDataManager.managedObjectContext
                                              shouldSkipSearch:YES
                                                  notOlderThan:self.prefs.deleteItemsOlderThan.unsignedIntValue];
                
                if (thisOne) {
                    totalCount++;
                    
                    if (currentSource.shouldAutoDownload.boolValue) {
                        [self downloadFeedLink:thisOne];
                        
                        if (currentSource.autoRead.boolValue) {
                            [thisOne markAsRead:[NSNumber numberWithBool:YES]];
                        }
                    }
                    
                    if (!currentSource.shouldAutoDownload.boolValue && self.prefs.useGrowl.boolValue && !self.prefs.groupGrowlNotifications.boolValue) {
                        
                        NSString *message = [NSString stringWithFormat:@"%@", thisOne.title];
                        
                        [GrowlApplicationBridge notifyWithTitle:[NSString stringWithFormat:@"%@", currentSource.name]
                                                    description:message
                                               notificationName:@"NewFeed"
                                                       iconData:thisOne.image
                                                       priority:0 isSticky:NO clickContext:nil];
                    }
                } else {
                    skipped++;
                }
            } else {
                skipped++;
            }
        }
        
        if (self.prefs.useGrowl.boolValue && self.prefs.groupGrowlNotifications.boolValue && self.prefs.groupSources.boolValue && [fetchedNews count] - skipped > 0) {
            // If notifications are grouped per source
            NSString *message;
            
            message = [NSString stringWithFormat:@"%ld new stories", [fetchedNews count] - skipped];
            
            [GrowlApplicationBridge notifyWithTitle:[NSString stringWithFormat:@"%@", currentSource.name]
                                        description:message
                                   notificationName:@"NewFeed"
                                           iconData:nil
                                           priority:0 isSticky:NO clickContext:nil];
        }
    }
    
    [localCoreDataManager saveData];
    
    if (self.prefs.useGrowl.boolValue && self.prefs.groupGrowlNotifications.boolValue && !self.prefs.groupSources.boolValue && totalCount > 0) {
        // If notifications are grouped all together
        
        NSString *message = [NSString stringWithFormat:@"%d stories were fetched from all the sources", totalCount];
        
        [GrowlApplicationBridge notifyWithTitle:@"RSS Reader"
                                    description:message
                               notificationName:@"NewFeed"
                                       iconData:nil
                                       priority:0 isSticky:NO clickContext:nil];
    }
    
    if (totalCount > 0) {
        if (self.prefs.bounceDockIconOnNewItems.boolValue) {
            [[NSApplication sharedApplication] requestUserAttention:NSInformationalRequest];
        }
    
        if (self.prefs.playSoundOnNewItems.boolValue) {
            [[NSSound soundNamed:@"Hero"] play];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"FeedParseEnd" object:self userInfo:nil];
}

- (void) mergeChanges: (NSNotification *)notification {
    iReadRSSAppDelegate *appController = (iReadRSSAppDelegate *)[[NSApplication sharedApplication] delegate];
    NSManagedObjectContext *context = [appController managedObjectContext];
    
    [context performSelectorOnMainThread:@selector(mergeChangesFromContextDidSaveNotification:) withObject:notification waitUntilDone:YES];
}

@end
